import XCTest

struct URLDataResponse: Decodable {
    var totalPages: Int
    var data : [Movie]
    
    enum CodingKeys : String, CodingKey {
        case totalPages = "total_pages"
        case data = "data"
    }
    
}

struct Movie: Decodable {
    public var title: String
    public var year: Int
    public var imdbID: String
    
    enum CodingKeys : String, CodingKey {
        case title = "Title"
        case year = "Year"
        case imdbID = "imdbID"
    }
}

enum APIError : Error {
    
    case InvalidURL
    case GenericError
    case InvalidResponse
    case Not200HTTPResponse
    case InvalidData
    case InvalidJSONData
    
}

class APIClient {
    
    private let baseStringURL = "https://jsonmock.hackerrank.com/api/movies/search/"
    
    private let urlSession = URLSession(configuration: .default)
    
    private let queryTitle = "Title"
    private let queryPage = "page"
    
    let group = DispatchGroup()
    
    func fetchTitles(with substr: String, completion: @escaping (Result<[Movie],APIError>)->(Void)) {
        
        self.fetchMovies(with: substr, page: "1") { result in
            
            switch result {
            case .success(let dataResponse):
                if dataResponse.totalPages > 1 {
                    self.fetchPages(substr: substr, pages: dataResponse.totalPages) { result in
                        
                        switch result {
                        case .success(let movies):
                            var allMovies = dataResponse.data
                            allMovies.append(contentsOf: movies)
                            completion(.success(allMovies))
                        case .failure(let error):
                            completion(.failure(error))
                        }
                    }
                    
                }else {
                    completion(.success(dataResponse.data))
                }
            case .failure(let error):
                completion(.failure(error))
            }
            
        }
        
    }
    
    func fetchPages(substr: String, pages: Int, completion: @escaping (Result<[Movie],APIError>) -> (Void)) {
        
        var movies : [Movie] = []
        for page in 2...pages {
            group.enter()
            fetchMovies(with: substr, page: String(page)) { result in
                switch result {
                case .success(let dataResponse):
                    defer{self.group.leave()}
                    movies.append(contentsOf: dataResponse.data)
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
        group.notify(queue: .global()) {
            completion(.success(movies))
        }
        
    }
    
    
    func fetchMovies(with substr: String, page: String, completion: @escaping ((Result<URLDataResponse, APIError>)) -> (Void)){
        
        guard var urlComponents = URLComponents(string: baseStringURL) else {
            return
        }
        
        let substrQueryItem = URLQueryItem(name: queryTitle, value: substr)
        let pageQueryItem = URLQueryItem(name: queryPage, value: page)
        
        urlComponents.queryItems = [substrQueryItem, pageQueryItem]
        
        guard let url = urlComponents.url else {
            return
        }
        
        urlSession.dataTask(with: url) { data, response, error in
            //MARK: - Error handling
            guard error == nil else {
                completion(.failure(.InvalidURL))
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                completion(.failure(.InvalidURL))
                return
            }
            
            guard response.statusCode >= 200 && response.statusCode <= 299 else {
                completion(.failure(.Not200HTTPResponse))
                return
            }
            
            guard let data = data else {
                completion(.failure(.InvalidData))
                return
            }
            
            //MARK: - Decode JSON data
            
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let dataResponse = try JSONDecoder().decode(URLDataResponse.self, from: data)
                completion(.success(dataResponse))
            }catch {
                completion(.failure(.InvalidJSONData))
            }
            
        }.resume()
    }
}

class Tests: XCTestCase {
    
    var movie: Movie!
    var api : APIClient!
    
    override func setUpWithError() throws{
        try super.setUpWithError()
        movie = Movie(title: "Movie", year: 2000, imdbID: "id")
        api = APIClient()
        
    }
    
    func testMovieModel() {
        XCTAssertNotNil(movie)
        XCTAssertEqual(movie.title, "Movie")
        XCTAssertEqual(movie.year, 2000)
        XCTAssertEqual(movie.imdbID, "id")
    }
    
    func testValidData() {
        
        let promise = expectation(description: "Success completion")
        api.fetchMovies(with: "spiderman", page: "1") { result in
            switch result {
            case .success(let response):
                promise.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
        wait(for: [promise], timeout: 5)
    }
}


//Tests.defaultTestSuite.run()

func movieTitles(from movies : [Movie]) -> [String] {
    
    var titles: [String] = []
    for movie in movies {
        titles.append(movie.title)
    }
    
    return titles
}

func fetchMovieTitles(with substr: String, completion: @escaping ([String]?) -> (Void)){
    
    APIClient().fetchTitles(with: substr) { result in
        switch result {
            
        case .success(let movies):
            var titles = movieTitles(from: movies)
            titles.sort { $0 < $1 }
            completion(titles)
        case .failure(let error):
            completion(nil)
        }
    }
}


fetchMovieTitles(with: "spiderman") { titles in
    
    guard var titles = titles else {
        return
    }
    
    print(titles)
    
}

